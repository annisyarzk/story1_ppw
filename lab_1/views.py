from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Annisya Rizkia Ifani' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000,11, 25) #TODO Implement this, format (Year, Month, Date)
npm = 1806191143 # TODO Implement this
kuliah = "Faculty of Computer Science, University of Indonesia"
angkatan = "Information Systems, 2018"
hobi = "Reading, Writting, Drinking coffee, Go to many concerts, and Travelling"
deskripsi_tentang_saya = "I am an optimistic and confident person"
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'kuliah': kuliah, 'hobi': hobi, 'deskripsi': deskripsi_tentang_saya, 'angkatan': angkatan}
    return render(request, 'index_lab1AWAL.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
